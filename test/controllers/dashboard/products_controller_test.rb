require 'test_helper'

class Dashboard::ProductsControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get dashboard_products_index_url
    assert_response :success
  end

end
