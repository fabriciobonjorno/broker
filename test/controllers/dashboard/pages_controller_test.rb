require 'test_helper'

class Dashboard::PagesControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get dashboard_pages_index_url
    assert_response :success
  end

end
