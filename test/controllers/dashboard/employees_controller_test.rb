require 'test_helper'

class Dashboard::EmployeesControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get dashboard_employees_index_url
    assert_response :success
  end

end
