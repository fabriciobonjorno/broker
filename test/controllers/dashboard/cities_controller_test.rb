require 'test_helper'

class Dashboard::CitiesControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get dashboard_cities_index_url
    assert_response :success
  end

end
