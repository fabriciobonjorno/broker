class Carousel < ApplicationRecord
    validates :description, :image, presence: true
    has_one_attached :image
end
