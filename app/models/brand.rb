class Brand < ApplicationRecord
  validates :description, uniqueness: true
  validates :description, :image, :line_id, presence: true
  has_one_attached :image
  belongs_to :line
  has_many :products


  def can_be_deleted?
    products.empty?
  end
end
