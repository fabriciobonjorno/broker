class Product < ApplicationRecord
  validates :description, uniqueness: true
  validates :description, :image, presence: true
  has_one_attached :image
  belongs_to :brand
end
