class City < ApplicationRecord
  validates :description, uniqueness: true
  validates :description, :employee_id, presence: true
  belongs_to :employee
end
