class Page < ApplicationRecord
    validates :title, :description, :image, presence: true
    has_one_attached :image
end
