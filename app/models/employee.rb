class Employee < ApplicationRecord
  validates :fullname, :cellphone, presence: true
  has_many :cities

  def can_be_deleted?
    cities.empty?
  end
end
