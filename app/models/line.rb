class Line < ApplicationRecord
    validates :description, uniqueness: true
    validates :description, :image, presence: true
    has_one_attached :image
end
