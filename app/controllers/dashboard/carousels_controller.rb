class Dashboard::CarouselsController < DashboardController
  before_action :set_carousel, only: %i[edit update destroy]
  def index
    @carousels = Carousel.all
  end

  def new
    @carousel = Carousel.new
  end

  def create
    @carousel = Carousel.new(params_carousel)
    if @carousel.save
      redirect_to dashboard_carousels_path, notice: "Destaque #{@carousel.description} cadastrado com sucesso"
    else
      flash.now[:alert]= @carousel.errors.full_messages.to_sentence
      render :new
    end
  end

  def edit

  end

  def update
    if @carousel.update(params_carousel)
      redirect_to dashboard_carousels_path, notice: "Destaque #{@carousel.description} atualizado com sucesso"
    else
      flash.now[:alert]= @carousel.errors.full_messages.to_sentence
      render :edit
    end
  end

  def destroy
    if @carousel.destroy
      redirect_to dashboard_carousels_path, notice: "Destaque #{@carousel.description} excluido com sucesso"
    else
      flash.now[:alert]= @carousel.errors.full_messages.to_sentence
      render :index
    end
  end

  private

  def set_carousel
    @carousel = Carousel.find(params[:id])
  end

  def params_carousel
    params.require(:carousel).permit(:description, :image)
  end
end