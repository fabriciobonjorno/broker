class Dashboard::ProductsController < DashboardController
  before_action :set_product, only: %i[edit update destroy]
  def index
    @products = Product.all
  end

  def new
    @product = Product.new
  end

  def create
    @product = Product.new(params_product)
    if @product.save
      redirect_to dashboard_products_path, notice: "O Produto #{@product.description} cadastrada com sucesso"
    else
      flash.now[:alert] = @product.errors.full_messagess.to_sentence
      render :new
    end
  end

  def edit

  end

  def update
    if @product.update(params_product)
      redirect_to dashboard_products_path, notice: "O produto #{@product.description} atualizada com sucesso"
    else
      flash.now[:alert]= @product.errors.full_messages.to_sentence
      render :edit
    end
  end

  def destroy
    if @product.destroy
      redirect_to dashboard_products_path, notice: "O produto #{@product.description} excluido com sucesso"
    else
      flash.now[:alert]= @product.errors.full_messages.to_sentence
      render :index
    end
  end

  private

  def set_product
    @product = Product.find(params[:id])
  end

  def params_product
    params.require(:product).permit(:description, :brand_id, :image)
  end
end

