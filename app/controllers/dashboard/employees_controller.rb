class Dashboard::EmployeesController < DashboardController
  before_action :set_employee, only: %i[edit update destroy]
  def index
    @employees = Employee.all
  end

  def new
    @employee = Employee.new
  end

  def create
    @employee = Employee.new(params_employee)
    if @employee.save
      redirect_to dashboard_employees_path, notice: "O funcionario #{@employee.fullname} cadastrado com sucesso"
    else
      flash.now[:alert]= @employee.errors.full_messages.to_sentence
      render :new
    end
  end

  def edit

  end

  def update
    if @employee.update(params_employee)
      redirect_to dashboard_employees_path, notice: "O funcionario #{@employee.fullname} atualizado com sucesso"
    else
      flash.now[:alert]= @employee.errors.full_messages.to_sentence
      render :edit
    end
  end

  def destroy
    if @employee.destroy
      redirect_to dashboard_employees_path, notice: "O funcionario #{@employee.fullname} excluido com sucesso"
    else
      flash.now[:alert]= @employee.errors.full_messages.to_sentence
      render :index
    end
  end

  private

  def set_employee
    @employee = Employee.find(params[:id])
  end

  def params_employee
    params.require(:employee).permit(:fullname, :cellphone)
  end
end