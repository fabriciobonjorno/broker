class Dashboard::CitiesController < DashboardController
  before_action :set_city, only: %i[edit update destroy]
  def index
    @cities = City.all
  end

  def new
    @city = City.new
  end

  def create
    @city = City.new(params_city)
    if @city.save
      redirect_to dashboard_cities_path, notice: "A cidade #{@city.description} cadastrada com sucesso!"
    else
      flash.now[:alert] = @city.errors.full_messages.to_sentence
      render :new
    end
  end

  def edit
  end

  def update
    if @city.update(params_city)
      redirect_to dashboard_cities_path, notice: "A cidade  #{@city.description} atualizada com sucesso!"
    else
      flash.now[:alert] = @city.errors.full_messages.to_sentence
      render :edit
    end
  end

  def destroy
    if @city.destroy
      redirect_to dashboard_cities_path, notice: "A cidade  #{@city.description} excluida com sucesso!"
    else
      flash.now[:alert] = @city.errors.full_messages.to_sentence
      render :index
    end
  end

  private

  def set_city
    @city = City.find(params[:id])
  end

  def params_city
    params.require(:city).permit(:description, :employee_id)
  end
end
