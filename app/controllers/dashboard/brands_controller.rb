class Dashboard::BrandsController < DashboardController
  before_action :set_brand, only: %i[edit update destroy]
  def index
    @brands = Brand.all
  end

  def new
    @brand = Brand.new
  end

  def create
    @brand = Brand.new(params_brand)
    if @brand.save
      redirect_to dashboard_brands_path, notice: "Marca #{@brand.description} cadastrada com sucesso"
    else
      flash.now[:alert] = @brand.errors.full_messages.to_sentence
      render :new
    end
  end

  def edit

  end

  def update
    if @brand.update(params_brand)
      redirect_to dashboard_brands_path, notice: "Marca #{@brand.description} atualizada com sucesso"
    else
      flash.now[:alert]= @brand.errors.full_messages.to_sentence
      render :edit
    end
  end

  def destroy
    if @brand.destroy
      redirect_to dashboard_brands_path, notice: "Marca #{@brand.description} excluido com sucesso"
    else
      flash.now[:alert]= @brand.errors.full_messages.to_sentence
      render :index
    end
  end

  private

  def set_brand
    @brand = Brand.find(params[:id])
  end

  def params_brand
    params.require(:brand).permit(:description, :line_id, :image)
  end
end
