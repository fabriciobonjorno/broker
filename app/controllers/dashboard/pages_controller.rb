class Dashboard::PagesController < DashboardController
  before_action :set_page, only: %i[edit update]
  def index
    @pages = Page.first
  end

  def edit

  end

  def update
    if @page.update(params_page)
      redirect_to dashboard_pages_path, notice: "#{@page.title} atualizada com sucesso"
    else
      flash.now[:alert]= @page.errors.full_messages.to_sentence
      render :edit
    end
  end

  private

  def set_page
    @page = Page.find(params[:id])
  end

  def params_page
    params.require(:page).permit(:title, :description, :image)
  end
end
