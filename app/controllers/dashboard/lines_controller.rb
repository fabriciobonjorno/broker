class Dashboard::LinesController < DashboardController
  before_action :set_line, only: %i[edit update]
  def index
    @lines = Line.all
  end

  def edit

  end

  def update
    if @line.update(params_line)
      redirect_to dashboard_lines_path, notice: "Linha #{@line.description} atualizada com sucesso"
    else
      flash.now[:alert]= @line.errors.full_messages.to_sentence
      render :edit
    end
  end

  private

  def set_line
    @line = Line.find(params[:id])
  end

  def params_line
    params.require(:line).permit(:description, :image)
  end
end