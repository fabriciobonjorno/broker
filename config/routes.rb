Rails.application.routes.draw do
  #get 'dashboard/index'
  get 'dashboard', to: 'dashboard#index'
  namespace :dashboard do
    resources :products, except: %i[show]
    resources :users, except: %i[show]
    resources :employees, except: %i[show]
    resources :cities, except: %i[show]
    resources :carousels, except: %i[show]
    resources :pages, except: %i[show new destroy]
    resources :lines, except: %i[show new destroy]
    resources :brands, except: %i[show]
  end

  devise_for :users, :skip => [:registration]
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
