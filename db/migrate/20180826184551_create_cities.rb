class CreateCities < ActiveRecord::Migration[5.2]
  def change
    create_table :cities do |t|
      t.string :description
      t.references :employee, foreign_key: true

      t.timestamps
    end
  end
end
